#include "Header.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h> 
#define DG 1
#define GG 12

void glavni_izbornik()
{
	int a;
	do
	{
		system("cls");  
		printf("Odaberite opciju:\n");
		printf("1. Program generira broj igraca\n");
		printf("2. Korisnik unosi broj igraca\n"); 
	    printf("Odabir:\n ");
		scanf("%d", &a);
	} while (a < 1 || a > 2);

	switch (a)
	{
	case 1:
		unos_igraca_program();
		break;
	case 2:
		unos_igraca_korisnik();
		break;
	}

}

void unos_igraca_program() 
{
	int a;
	a = rand() % GG + DG; 
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "wb"); 
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	else
	{
		fwrite(&a, sizeof(int), 1, fp);  
		IGRAC* igrac = NULL;
		igrac = (IGRAC*)calloc(a, sizeof(IGRAC));
		if (igrac == NULL)
		{
			printf("Greska!");
			exit(EXIT_FAILURE);
		}

		for (int i = 0; i < a; i++)
		{
			system("cls");
			printf("Unosi se %d / %d igrac(a)\n", i+1, a);
			printf("Unesite ime %d. igraca: \n", i + 1);
			scanf("%20s", &(igrac + i)->ime);
			printf("Unesite godine %d. igraca: \n ", i + 1);
			scanf("%d", &(igrac + i)->godine);
			printf("Unesite broj poena %d. igraca: \n", i + 1);
			scanf("%d", &(igrac + i)->broj_poena);
			printf("Unesite broj utakmica %d. igraca: \n", i + 1);
			scanf("%d", &(igrac + i)->broj_utakmica);
			printf("Unesite broj igraca %d. igraca: \n", i + 1);
			scanf("%d", &(igrac + i)->broj_igraca);
			(igrac + i)->prosjek = (float)(igrac + i)->broj_poena / (float)(igrac + i)->broj_utakmica;
		}
		fwrite(igrac, sizeof(IGRAC), a, fp); 
		_getch();
		free(igrac); 
		fclose(fp); 
		malo_manje_glavni_izbornik(); 
	}
}

void unos_igraca_korisnik() 
{
	int a;
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "wb"); 
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	else
	{
		do
		{
			system("cls");
			printf("Unesite broj igraca:\n");
			scanf("%d", &a);
		} while (a < 1 || a > 12);
		fwrite(&a, sizeof(int), 1, fp); 
		IGRAC* igrac = NULL;
		igrac = (IGRAC*)calloc(a, sizeof(IGRAC));
		if (igrac == NULL)
		{
			printf("Greska!");
			exit(EXIT_FAILURE);
		}

		for (int i = 0; i < a; i++)
		{
			system("cls");
			printf("Unosi se %d / %d igrac(a)\n", i + 1, a);
			printf("Unesite ime %d. igraca: \n ", i + 1);
			scanf("%20s", &(igrac + i)->ime);
			printf("Unesite godine %d. igraca: \n", i + 1);
			scanf("%d", &(igrac + i)->godine);
			printf("Unesite broj poena %d. igraca: \n", i + 1);
			scanf("%d", &(igrac + i)->broj_poena);
			printf("Unesite broj utakmica %d. igraca: \n", i + 1);
			scanf("%d", &(igrac + i)->broj_utakmica);
			printf("Unesite broj igraca %d. igraca: \n", i + 1);
			scanf("%d", &(igrac + i)->broj_igraca);
			(igrac + i)->prosjek = (float)(igrac + i)->broj_poena / (float)(igrac + i)->broj_utakmica;
		}
		fwrite(igrac, sizeof(IGRAC), a, fp);
		_getch();
		free(igrac);
		fclose(fp);
		malo_manje_glavni_izbornik();
	}
}

void malo_manje_glavni_izbornik() 
{
	int a;
	while (1)
	{
		do
		{
			system("cls");
			printf("Odaberite opciju:\n");
			printf("1. Ispis datoteke\n");
			printf("2.Ispis po parametrima\n");
			printf("3.Pretraga datoteke\n");
			printf("4.Brisanje iz datoteke\n");
			printf("5.Izlaz\n"); 
		    printf("Odabir: \n");
			scanf("%d", &a);
		} while (a < 1 || a > 5);
		switch (a)
		{
		case 1:
			ispis_datoteke();
			break;
		case 2:
			ispis_parametri();
			break;
		case 3:
			pretraga_datoteke();
			break;
		case 4:
			brisanje_iz_datoteke();
			break;
		case 5:
			exit(EXIT_SUCCESS);
		}
	}
}

void ispis_datoteke()  
{
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "rb"); 
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	IGRAC* igrac = NULL;
	int a;
	fread(&a, sizeof(int), 1, fp);
	igrac = (IGRAC*)calloc(a, sizeof(IGRAC)); 
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	fread(igrac, sizeof(IGRAC), a, fp);

	system("cls");
	for (int i = 0; i < a; i++)
	{
		printf("Ispis %d. / %d igraca:\n", i + 1, a); 
		printf("Ime: %s\n", (igrac + i)->ime);
		printf("Godine: %d\n", (igrac + i)->godine);
		printf("Broj poena: %d\n", (igrac + i)->broj_poena);
		printf("Broj utakmica: %d\n", (igrac + i)->broj_utakmica);
		printf("Broj igraca: %d\n", (igrac + i)->broj_igraca);
		printf("Prosjek igraca: %f\n", (igrac + i)->prosjek);
		_getch();
	}
	fclose(fp);
	free(igrac);
	return;
}

void pretraga_datoteke()
{
	int a;
	while (1) 
	{
		do
		{
			system("cls");
			printf("Pretraga:\n");
			printf("1. Po imenu\n");
			printf("2. Po godinama\n");
			printf("3.Po broju poena\n");
			printf("4.Po broju utakmica\n");
			printf("5.Po broju igraca\n");
			printf("6.Povratak\n");
	        printf("Odabir: \n");
			scanf("%d", &a);
		} while (a < 1 || a > 6);
		switch (a)
		{
		case 1:
			pretraga_po_imenu();
			break;
		case 2:
			pretraga_po_godinama();
			break;
		case 3:
			pretraga_po_br_poena();
			break;
		case 4:
			pretraga_po_br_utakmica();
			break;
		case 5:
			pretraga_po_br_igraca();
			break;
		case 6:
			return; 
		}
	}
}

void brisanje_iz_datoteke()
{
	int a;
	while (1) 
	{
		do
		{
			system("cls");
			printf("Brisanje:\n");
			printf("1. Po imenu\n");
			printf("2. Po godinama\n");
			printf("3.Po broju poena\n");
			printf("4.Po broju utakmica\n");
			printf("5.Po broju igraca\n");
			printf("6.Povratak\n");
		    printf("Odabir: \n");
			scanf("%d", &a);
		} while (a < 1 || a > 6);
		switch (a)
		{
		case 1:
			brisanje_po_imenu();
			break;
		case 2:
			brisanje_po_godinama();
			break;
		case 3:
			brisanje_po_br_poena();
			break;
		case 4:
			brisanje_po_br_utakmica();
			break;
		case 5:
			brisanje_po_br_igraca();
			break;
		case 6:
			return; 
		}
	}
}

void ispis_parametri()
{
	int a;
	while (1)  
	{
		do
		{
			system("cls");
			printf("Ispis po parametrima\n");
			printf("1. Najstariji\n");
			printf("2.Najmladji\n");
			printf("3.Najvise poena\n");
			printf("4.Najmanje poena\n");
			printf("5.Najvise utakmica\n");
			printf("6.Najmanje utakmica\n");
			printf("7.Najveci prosjek\n");
			printf("8.Najmanji prosjek\n");
			printf("9.Povratak\n");
		    printf("Odabir: \n");
			scanf("%d", &a);
		} while (a < 1 || a > 9);
		switch (a)
		{
		case 1:
			godine_ispis(1);
			break;
		case 2:
			godine_ispis(2);
			break;
		case 3:
			poeni_ispis(1);
			break;
		case 4:
			poeni_ispis(2);
			break;
		case 5:
			utakmica_ispis(1);
			break;
		case 6:
			utakmica_ispis(2);
			break;
		case 7:
			prosjek_ispis(1);
			break;
		case 8:
			prosjek_ispis(2);
			break;
		case 9:
			return; 
		}
	}
}