#ifndef HEADER_H
#define HEADER_H
#define _CRT_SECURE_NO_WARNINGS

typedef struct igrac 
{
    char ime[20];
    int godine;
    int broj_poena;
    int broj_utakmica;
    int broj_igraca;
    float prosjek;
} IGRAC;

void glavni_izbornik();
void unos_igraca_program();
void unos_igraca_korisnik();
void malo_manje_glavni_izbornik();
void ispis_datoteke();
void pretraga_datoteke();
void brisanje_iz_datoteke();
void ispis_parametri();

void pretraga_po_imenu();
void pretraga_po_godinama();
void pretraga_po_br_poena();
void pretraga_po_br_utakmica();
void pretraga_po_br_igraca();

void brisanje_po_imenu();
void brisanje_po_godinama();
void brisanje_po_br_poena();
void brisanje_po_br_utakmica();
void brisanje_po_br_igraca();

void godine_ispis(int);
void poeni_ispis(int);
void utakmica_ispis(int);
void prosjek_ispis(int);

#endif