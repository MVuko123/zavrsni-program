#include "Header.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>

void brisanje_po_imenu()
{
	FILE* fp;
	FILE* fp_tmp;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	fp_tmp = fopen("pomocna.bin", "wb"); 
	if (fp_tmp == 0)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}

	char* ime = NULL;
	ime = (char*)calloc(20, sizeof(char));
	if (ime == NULL) {
		printf("Greska!");
		exit(EXIT_FAILURE);
	}

	system("cls");
	printf("Brisanje po imenu\n");
	printf("Unesite ime : \n");
	scanf("%20s", ime);

	int pronadeno = 0, broj_igraca, pomocna;

	fread(&broj_igraca, sizeof(int), 1, fp);

	IGRAC* igrac = NULL;
	igrac = (IGRAC*)calloc(broj_igraca, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greksa!");
		exit(EXIT_FAILURE);
	}

	pomocna = broj_igraca - 1;
	fwrite(&pomocna, sizeof(int), 1, fp_tmp);

	for (int i = 0; i < broj_igraca; i++) 
	{
		fread(igrac, sizeof(IGRAC), 1, fp);
		if (strcmp(igrac->ime, ime) == 0)
		{
			pronadeno = 1;                             
		}
		else
		{
			fwrite(igrac, sizeof(IGRAC), 1, fp_tmp);
		}
	}

	if (pronadeno == 0)
	{
		printf("Nema igraca s imenom %s\n", ime);
	}

	fclose(fp);
	fclose(fp_tmp);

	remove("igraci.bin");
	rename("pomocna.bin", "igraci.bin");

	free(igrac);
	free(ime);

	return;
}

void brisanje_po_godinama()
{
	FILE* fp;
	FILE* fp_tmp;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	fp_tmp = fopen("pomocna.bin", "wb");
	if (fp_tmp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}

	int godine;

	system("cls");
	printf("Brisanje po godinama\n");
    printf("Unesite godine: \n");
	scanf("%d", &godine);

	int pronadeno = 0, broj_igraca, pomocna;

	fread(&broj_igraca, sizeof(int), 1, fp);

	IGRAC* igrac = NULL;
	igrac = (IGRAC*)calloc(broj_igraca, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	int broji_obrisane = 0;
	pomocna = broj_igraca;
	fwrite(&pomocna, sizeof(int), 1, fp_tmp);

	for (int i = 0; i < broj_igraca; i++)
	{
		fread(igrac, sizeof(IGRAC), 1, fp);
		if (igrac->godine == godine)
		{
			broji_obrisane++;
			pronadeno = 1;
		}
		else
		{
			fwrite(igrac, sizeof(IGRAC), 1, fp_tmp);
		}
	}

	pomocna = broj_igraca - broji_obrisane; 
	rewind(fp_tmp);
	fwrite(&pomocna, sizeof(int), 1, fp_tmp);

	if (pronadeno == 0)
	{
		printf("Nema igraca s godinama %d\n", godine);
	}

	fclose(fp);
	fclose(fp_tmp);

	remove("igraci.bin");
	rename("pomocna.bin", "igraci.bin");

	free(igrac);

	return;
}

void brisanje_po_br_poena()
{
	FILE* fp;
	FILE* fp_tmp;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	fp_tmp = fopen("pomocna.bin", "wb");
	if (fp_tmp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}

	int broj_poena;

	system("cls");
	printf("Brisanje po broju poena\n");
	printf("Unesite broj poena : \n");
	scanf("%d", &broj_poena);

	int pronadeno = 0, broj_igraca, pomocna;

	fread(&broj_igraca, sizeof(int), 1, fp);

	IGRAC* igrac = NULL;
	igrac = (IGRAC*)calloc(broj_igraca, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	int broji_obrisane = 0;
	pomocna = broj_igraca;
	fwrite(&pomocna, sizeof(int), 1, fp_tmp);

	for (int i = 0; i < broj_igraca; i++)
	{
		fread(igrac, sizeof(IGRAC), 1, fp);
		if (igrac->broj_poena == broj_poena)
		{
			broji_obrisane++;
			pronadeno = 1;
		}
		else
		{
			fwrite(igrac, sizeof(IGRAC), 1, fp_tmp);
		}
	}

	pomocna = broj_igraca - broji_obrisane;
	rewind(fp_tmp);
	fwrite(&pomocna, sizeof(int), 1, fp_tmp);

	if (pronadeno == 0)
	{
		printf("Nema igraca s brojem poena %d\n", broj_poena);
	}

	fclose(fp);
	fclose(fp_tmp);

	remove("igraci.bin");
	rename("pomocna.bin", "igraci.bin");

	free(igrac);

	return;
}

void brisanje_po_br_utakmica()
{
	FILE* fp;
	FILE* fp_tmp;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	fp_tmp = fopen("pomocna.bin", "wb");
	if (fp_tmp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}

	int broj_utakmica;

	system("cls");
	printf("Brisanje po broju utakmica\n");
	printf("Unesite broj utakmica : \n");
	scanf("%d", &broj_utakmica);

	int pronadeno = 0, broj_igraca, pomocna;

	fread(&broj_igraca, sizeof(int), 1, fp);

	IGRAC* igrac = NULL;
	igrac = (IGRAC*)calloc(broj_igraca, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	int broji_obrisane = 0;
	pomocna = broj_igraca;
	fwrite(&pomocna, sizeof(int), 1, fp_tmp);

	for (int i = 0; i < broj_igraca; i++)
	{
		fread(igrac, sizeof(IGRAC), 1, fp);
		if (igrac->broj_utakmica == broj_utakmica)
		{
			broji_obrisane++;
			pronadeno = 1;
		}
		else
		{
			fwrite(igrac, sizeof(IGRAC), 1, fp_tmp);
		}
	}

	pomocna = broj_igraca - broji_obrisane;
	rewind(fp_tmp);
	fwrite(&pomocna, sizeof(int), 1, fp_tmp);

	if (pronadeno == 0)
	{
		printf("Nema igraca s brojem utakmica %d\n", broj_utakmica);
	}

	fclose(fp);
	fclose(fp_tmp);

	remove("igraci.bin");
	rename("pomocna.bin", "igraci.bin");

	free(igrac);

	return;
}

void brisanje_po_br_igraca()
{
	FILE* fp;
	FILE* fp_tmp;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	fp_tmp = fopen("pomocna.bin", "wb");
	if (fp_tmp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}

	int igracev_broj;

	system("cls");
	printf("Brisanje po broju igraca\n");
    printf("Unesite broj igraca:\n ");
	scanf("%d", &igracev_broj);

	int pronadeno = 0, broj_igraca, pomocna;

	fread(&broj_igraca, sizeof(int), 1, fp);

	IGRAC* igrac = NULL;
	igrac = (IGRAC*)calloc(broj_igraca, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	int broji_obrisane = 0;
	pomocna = broj_igraca;
	fwrite(&pomocna, sizeof(int), 1, fp_tmp);

	for (int i = 0; i < broj_igraca; i++)
	{
		fread(igrac, sizeof(IGRAC), 1, fp);
		if (igrac->broj_igraca == igracev_broj)
		{
			broji_obrisane++;
			pronadeno = 1;
		}
		else
		{
			fwrite(igrac, sizeof(IGRAC), 1, fp_tmp);
		}
	}

	pomocna = broj_igraca - broji_obrisane;
	rewind(fp_tmp);
	fwrite(&pomocna, sizeof(int), 1, fp_tmp);

	if (pronadeno == 0)
	{
		printf("Nema igraca s brojem %d\n", igracev_broj);
	}

	fclose(fp);
	fclose(fp_tmp);

	remove("igraci.bin");
	rename("pomocna.bin", "igraci.bin");

	free(igrac);

	return;
}