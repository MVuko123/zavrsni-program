#include "Header.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

void godine_ispis(int a)
{
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	int broj_igraca;
	fread(&broj_igraca, sizeof(int), 1, fp);
	IGRAC* igrac = NULL;
	igrac = (IGRAC*)calloc(broj_igraca, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}

	fread(igrac, sizeof(IGRAC), broj_igraca, fp);
	if (a == 1)
	{
		int najstariji = 0, pomocna = 0;
		for (int i = 0; i < broj_igraca; i++)
		{
			if ((igrac + i)->godine > najstariji)
			{
				najstariji = (igrac + i)->godine;
				pomocna = i;
			}
		}
		system("cls");
		printf("Najstariji igrac:\n");
	    printf("Ime:%s \n", (igrac + pomocna)->ime);
		printf("Godine: %d\n", (igrac + pomocna)->godine);
		printf("Broj poema: %d\n", (igrac + pomocna)->broj_poena);
		printf("Broj utakmica: %d\n", (igrac + pomocna)->broj_utakmica);
		printf("Broj igraca: %d\n", (igrac + pomocna)->broj_igraca);
		printf("Prosjek igraca: %f\n", (igrac + pomocna)->prosjek);
		_getch();;
		fclose(fp);
		free(igrac);
		return;
	}
	else
	{
		int najmladji, pomocna = 0;
		najmladji = (igrac + 0)->godine;
		for (int i = 0; i < broj_igraca; i++)
		{
			if ((igrac + i)->godine < najmladji)
			{
				najmladji = (igrac + i)->godine;
				pomocna = i;
			}
		}
		system("cls");
		printf("Najmladji igrac\n");
   	    printf("Ime:%s \n", (igrac + pomocna)->ime);
		printf("Godine: %d\n", (igrac + pomocna)->godine);
		printf("Broj poema: %d\n", (igrac + pomocna)->broj_poena);
		printf("Broj utakmica: %d\n", (igrac + pomocna)->broj_utakmica);
		printf("Broj igraca: %d\n", (igrac + pomocna)->broj_igraca);
		printf("Prosjek igraca: %f\n", (igrac + pomocna)->prosjek);
		_getch();;
		fclose(fp);
		free(igrac);
		return;
	}
}

void poeni_ispis(int a)
{
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	int broj_igraca;
	fread(&broj_igraca, sizeof(int), 1, fp);
	IGRAC* igrac = NULL;
	igrac = (IGRAC*)calloc(broj_igraca,  sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}

	fread(igrac, sizeof(IGRAC), broj_igraca, fp);
	if (a == 1)
	{
		int najvise = 0, pomocna = 0;
		for (int i = 0; i < broj_igraca; i++)
		{
			if ((igrac + i)->broj_poena > najvise)
			{
				najvise = (igrac + i)->broj_poena;
				pomocna = i;
			}
		}
		system("cls");
		printf("Najvise poena\n");
	    printf("Ime:%s \n", (igrac + pomocna)->ime);
		printf("Godine: %d\n", (igrac + pomocna)->godine);
		printf("Broj poema: %d\n", (igrac + pomocna)->broj_poena);
		printf("Broj utakmica: %d\n", (igrac + pomocna)->broj_utakmica);
		printf("Broj igraca: %d\n", (igrac + pomocna)->broj_igraca);
		printf("Prosjek igraca: %f\n", (igrac + pomocna)->prosjek);
		_getch();;
		fclose(fp);
		free(igrac);
		return;
	}
	else
	{
		int najmanje, pomocna = 0;
		najmanje = (igrac + 0)->broj_poena;
		for (int i = 0; i < broj_igraca; i++)
		{
			if ((igrac + i)->broj_poena < najmanje)
			{
				najmanje = (igrac + i)->broj_poena;
				pomocna = i;
			}
		}
		system("cls");
		printf("Najmanje poena\n");
	    printf("Ime:%s \n", (igrac + pomocna)->ime);
		printf("Godine: %d\n", (igrac + pomocna)->godine);
		printf("Broj poema: %d\n", (igrac + pomocna)->broj_poena);
		printf("Broj utakmica: %d\n", (igrac + pomocna)->broj_utakmica);
		printf("Broj igraca: %d\n", (igrac + pomocna)->broj_igraca);
		printf("Prosjek igraca: %f\n", (igrac + pomocna)->prosjek);
		_getch();;
		fclose(fp);
		free(igrac);
		return;
	}
}

void utakmica_ispis(int a)
{
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	int broj_igraca;
	fread(&broj_igraca, sizeof(int), 1, fp);
	IGRAC* igrac = NULL;
	igrac = (IGRAC*)calloc(broj_igraca, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}

	fread(igrac, sizeof(IGRAC), broj_igraca, fp);
	if (a == 1)
	{
		int najvise = 0, pomocna = 0;
		for (int i = 0; i < broj_igraca; i++)
		{
			if ((igrac + i)->broj_utakmica > najvise)
			{
				najvise = (igrac + i)->broj_utakmica;
				pomocna = i;
			}
		}
		system("cls");
		printf("Najvise utakmica\n"); 
    	printf("Ime:%s \n", (igrac + pomocna)->ime);
		printf("Godine: %d\n", (igrac + pomocna)->godine);
		printf("Broj poema: %d\n", (igrac + pomocna)->broj_poena);
		printf("Broj utakmica: %d\n", (igrac + pomocna)->broj_utakmica);
		printf("Broj igraca: %d\n", (igrac + pomocna)->broj_igraca);
		printf("Prosjek igraca: %f\n", (igrac + pomocna)->prosjek);
		_getch();;
		fclose(fp);
		free(igrac);
		return;
	}
	else
	{
		int najmanje, pomocna = 0;
		najmanje = (igrac + 0)->broj_utakmica;
		for (int i = 0; i < broj_igraca; i++)
		{
			if ((igrac + i)->broj_utakmica < najmanje)
			{
				najmanje = (igrac + i)->broj_utakmica;
				pomocna = i;
			}
		}
		system("cls");
		printf("Najmanje utakmica\n"); 
	    printf("Ime:%s \n", (igrac + pomocna)->ime);
		printf("Godine: %d\n", (igrac + pomocna)->godine);
		printf("Broj poema: %d\n", (igrac + pomocna)->broj_poena);
		printf("Broj utakmica: %d\n", (igrac + pomocna)->broj_utakmica);
		printf("Broj igraca: %d\n", (igrac + pomocna)->broj_igraca);
		printf("Prosjek igraca: %f\n", (igrac + pomocna)->prosjek);
		_getch();;
		fclose(fp);
		free(igrac);
		return;
	}
}

void prosjek_ispis(int a)
{
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	int broj_igraca;
	fread(&broj_igraca, sizeof(int), 1, fp);
	IGRAC* igrac = NULL;
	igrac = (IGRAC*)calloc(broj_igraca, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}

	fread(igrac, sizeof(IGRAC), broj_igraca, fp);
	if (a == 1)
	{
		int pomocna = 0;
		float najvise = 0.0;
		for (int i = 0; i < broj_igraca; i++)
		{
			if ((igrac + i)->prosjek > najvise)
			{
				najvise = (igrac + i)->prosjek;
				pomocna = i;
			}
		}
		system("cls");
		printf("Najveci prosjek\n"); 
	    printf("Ime:%s \n", (igrac + pomocna)->ime);
		printf("Godine: %d\n", (igrac + pomocna)->godine);
		printf("Broj poema: %d\n", (igrac + pomocna)->broj_poena);
		printf("Broj utakmica: %d\n", (igrac + pomocna)->broj_utakmica);
		printf("Broj igraca: %d\n", (igrac + pomocna)->broj_igraca);
		printf("Prosjek igraca: %f\n", (igrac + pomocna)->prosjek);
		_getch();;
		fclose(fp);
		free(igrac);
		return;
	}
	else
	{
		int pomocna = 0;
		float najmanje;
		najmanje = (igrac + 0)->prosjek;
		for (int i = 0; i < broj_igraca; i++)
		{
			if ((igrac + i)->prosjek < najmanje)
			{
				najmanje = (igrac + i)->prosjek;
				pomocna = i;
			}
		}
		system("cls");
		printf("Najmanji prosjek\n"); 
	    printf("Ime:%s \n", (igrac + pomocna)->ime);
		printf("Godine: %d\n", (igrac + pomocna)->godine);
		printf("Broj poema: %d\n", (igrac + pomocna)->broj_poena);
		printf("Broj utakmica: %d\n", (igrac + pomocna)->broj_utakmica);
		printf("Broj igraca: %d\n", (igrac + pomocna)->broj_igraca);
		printf("Prosjek igraca: %f\n", (igrac + pomocna)->prosjek);
		_getch();;
		fclose(fp);
		free(igrac);
		return;
	}
}