#include "Header.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h> 
#include <string.h>

void pretraga_po_imenu()
{
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	IGRAC* igrac = NULL;
	int a, pomocna = 0;
	fread(&a, sizeof(int), 1, fp);
	igrac = (IGRAC*)calloc(a, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	fread(igrac, sizeof(IGRAC), a, fp);

	char* ime;
	ime = (char*)calloc(20, sizeof(char));
	if (ime == NULL) {
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	system("cls");
	printf("Pretraga po imenu\n");
	printf("Unesite ime: \n ");
	scanf("%20s", ime);

	for (int i = 0; i < a; i++)
	{
		if (strcmp((igrac + i)->ime, ime) == 0) 
		{
			printf("Ime: %s\n", (igrac + i)->ime);
			printf("Godine: %d\n", (igrac + i)->godine);
			printf("Broj poema: %d\n", (igrac + i)->broj_poena);
			printf("Broj utakmica: %d\n", (igrac + i)->broj_utakmica);
			printf("Broj igraca: %d\n", (igrac + i)->broj_igraca);
			printf("Prosjek igraca: %f\n", (igrac + i)->prosjek);
			_getch();
			pomocna = 1;
			break;
		}
	}
	if (pomocna == 0)
	{
		printf("Igrac nije pronadjen!\n");
		_getch();
	}
	free(ime);
	fclose(fp);
	free(igrac);
	return;
}

void pretraga_po_godinama()
{
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	IGRAC* igrac = NULL;
	int a, pomocna = 0;
	fread(&a, sizeof(int), 1, fp);
	igrac = (IGRAC*)calloc(a, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Struktura");
		exit(EXIT_FAILURE);
	}
	fread(igrac, sizeof(IGRAC), a, fp);

	int godine;
	system("cls");
	printf("Pretraga po godinama\n");
	printf("Unesite godine : \n");
	scanf("%d", &godine);

	for (int i = 0; i < a; i++)
	{
		if ((igrac + i)->godine == godine)
		{
			printf("Ime: %s\n", (igrac + i)->ime);
			printf("Godine: %d\n", (igrac + i)->godine);
			printf("Broj poema: %d\n", (igrac + i)->broj_poena);
			printf("Broj utakmica: %d\n", (igrac + i)->broj_utakmica);
			printf("Broj igraca: %d\n", (igrac + i)->broj_igraca);
			printf("Prosjek igraca: %f\n", (igrac + i)->prosjek);
			_getch();
			pomocna = 1;
			break;
		}
	}
	if (pomocna == 0)
	{
		printf("Igrac nije pronadjen!\n");
		_getch();;
	}
	fclose(fp);
	free(igrac);
	return;
}

void pretraga_po_br_poena()
{
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	IGRAC* igrac = NULL;
	int a, pomocna = 0;
	fread(&a, sizeof(int), 1, fp);
	igrac = (IGRAC*)calloc(a, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	fread(igrac, sizeof(IGRAC), a, fp);

	int broj_poena;
	system("cls");
	printf("Pretraga po broju poena\n");
	printf("Unesite broj poena : \n");
	scanf("%d", &broj_poena);

	for (int i = 0; i < a; i++)
	{
		if ((igrac + i)->broj_poena == broj_poena)
		{
			printf("Ime: %s\n", (igrac + i)->ime);
			printf("Godine: %d\n", (igrac + i)->godine);
			printf("Broj poema: %d\n", (igrac + i)->broj_poena);
			printf("Broj utakmica: %d\n", (igrac + i)->broj_utakmica);
			printf("Broj igraca: %d\n", (igrac + i)->broj_igraca);
			printf("Prosjek igraca: %f\n", (igrac + i)->prosjek);
			_getch();
			pomocna = 1;
			break;
		}
	}
	if (pomocna == 0)
	{
		printf("Igrac nije pronadjen!\n");
		_getch();;
	}
	fclose(fp);
	free(igrac);
	return;
}

void pretraga_po_br_utakmica()
{
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	IGRAC* igrac = NULL;
	int a, pomocna = 0;
	fread(&a, sizeof(int), 1, fp);
	igrac = (IGRAC*)calloc(a, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	fread(igrac, sizeof(IGRAC), a, fp);

	int broj_utakmica;
	system("cls");
	printf("Pretraga po broju utakmica\n");
	printf("Unesite broj utakmica : \n ");
	scanf("%d", &broj_utakmica);

	for (int i = 0; i < a; i++)
	{
		if ((igrac + i)->broj_utakmica == broj_utakmica)
		{
			printf("Ime: %s\n", (igrac + i)->ime);
			printf("Godine: %d\n", (igrac + i)->godine);
			printf("Broj poema: %d\n", (igrac + i)->broj_poena);
			printf("Broj utakmica: %d\n", (igrac + i)->broj_utakmica);
			printf("Broj igraca: %d\n", (igrac + i)->broj_igraca);
			printf("Prosjek igraca: %f\n", (igrac + i)->prosjek);
			_getch();
			pomocna = 1;
			break;
		}
	}
	if (pomocna == 0)
	{
		printf("Igrac nije pronadjen!\n");
		_getch();;
	}
	fclose(fp);
	free(igrac);
	return;
}

void pretraga_po_br_igraca()
{
	FILE* fp = NULL;
	fp = fopen("igraci.bin", "rb");
	if (fp == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	IGRAC* igrac = NULL;
	int a, pomocna = 0;
	fread(&a, sizeof(int), 1, fp);
	igrac = (IGRAC*)calloc(a, sizeof(IGRAC));
	if (igrac == NULL)
	{
		printf("Greska!");
		exit(EXIT_FAILURE);
	}
	fread(igrac, sizeof(IGRAC), a, fp);

	int broj_igraca;
	system("cls");
	printf("Pretraga po broju igraca\n");
	printf("Unesite broj igraca: \n");
	scanf("%d", &broj_igraca);

	for (int i = 0; i < a; i++)
	{
		if ((igrac + i)->broj_igraca == broj_igraca)
		{
			printf("Ime: %s\n", (igrac + i)->ime);
			printf("Godine: %d\n", (igrac + i)->godine);
			printf("Broj poema: %d\n", (igrac + i)->broj_poena);
			printf("Broj utakmica: %d\n", (igrac + i)->broj_utakmica);
			printf("Broj igraca: %d\n", (igrac + i)->broj_igraca);
			printf("Prosjek igraca: %f\n", (igrac + i)->prosjek);
			_getch();
			pomocna = 1;
			break;
		}
	}
	if (pomocna == 0)
	{
		printf("Igrac nije pronadjen!\n");
		_getch();;
	}
	fclose(fp);
	free(igrac);
	return;
}